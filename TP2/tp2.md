# TP Léo Réseau n2

## I - ARP

### 1.Echange ARP

* Une fois les deux VM's dans le même réseau, il est possible de les faire communiquer avec un ping.
```
[bob@node2 ~]$ ping vm1
[...]
12 packets transmitted, 12 received, 0% packet loss, time 11083ms
```
* Pour observer la table ARP, on utilise la commande ```arp -a```
* En faisant la commande ```arp -a``` sur node1, on obtient : ```vm2 (10.2.1.12) at 08:00:27:ee:19:f3 [ether] on enp0s8```, l'adresse MAC de node2 est donc : 08:00:27:ee:19:f3.
* Sur node2, on obtient : ```vm1 (10.2.1.11) at 08:00:27:44:70:44 [ether] on enp0s8```, l'adresse MAC de node1 est donc : 08:00:27:44:70:44.
* Pour prouver que l'info est correcte : 
    * on utilise la commande ```arp -a``` sur node1 pour mettre en évidence la ligne au dessus, et voir la possible adresse MAC de node 2. On obtient donc ```08:00:27:ee:19:f3```.
    * on utilise la commande ```ip a``` sur node2 pour afficher les informations de la carte enp0s8. On obtient bel et bien la même adresse MAC au niveau de la carte enp0s8.
On a donc bien prouvé que les informations sont correctes, et que les machines ont bel et bien communiquées.

### 2. Analyse de trames

* On utilise la commande ```sudo tcpdump -i enp0s8 -w tp2_arp.pcap not port 22``` afin de réaliser la capture de trames. Pendant la capture, on réalise un ping depuis l'autre VM, avant d'arrêter le ping, puis la capture. On se retrouve avec le fichier ```tp2_routage-router.pcap``` qui contient les trames analysées. On doit ensuite exporter ce fichier sur notre machine, en utilisant la commande ```scp user@ipMachine:/home/user/tp2_arp.pcap PathLocal``` en remplaçant user, ipMachine et PathLocal. 

* Maintenant que le fichier est sur mon pc, je peux l'ouvrir sur Wireshark et obtenir la capture des trames. Voici les trames ARP récupérées.

```
| n° |   time   |       Source      |    Destination    | Protocol |Length|               Info                |
|--- |    :-:   |        :-:        |        :-:        |    :-:   |  :-: |                :-:                |
| 13 | 5.019466 | PcsCompu_44:70:44 | PcsCompu_ee:19:f3 |    ARP   |  42  | Who has 10.2.1.12? Tell 10.2.1.11 |
| 14 | 5.020291 | PcsCompu_ee:19:f3 | PcsCompu_44:70:44 |    ARP   |  60  | 10.2.1.12 is at 08:00:27:ee:19:f3 |
| 15 | 5.169866 | PcsCompu_ee:19:f3 | PcsCompu_44:70:44 |    ARP   |  60  | Who has 10.2.1.11? Tell 10.2.1.12 |
| 16 | 5.169888 | PcsCompu_44:70:44 | PcsCompu_ee:19:f3 |    ARP   |  42  | 10.2.1.11 is at 08:00:27:44:70:44 |
```

## II - Routage

### 1. Mise en place du Routage

Pour la mise en place des VMs, il faut, comme dans la première partie du TP, faire les choses suivantes : 
* créer les machines marcel et router (à partir du patron) en faisant bien attention à régénérer de nouvelles adresses MAC.
* changer le nom de la machine
* modifier l'ip de la carte enp0s8 pour marcel, et les cartes enp0s8 et enp0s9 pour notre routeur avec la commande ```sudo nano /etc/sysconfig/network-scripts/ifcfg-<nomdecarte>```. Il faudra, dans le cas de enp0s9, créer le fichier de enp0s9 qui est vide par defaut, et mettre les informations requises à l'intérieur du fichier.
* reload les différentes cartes, avec les commandes ```sudo nmcli con reload``` puis ```sudo nmcli con up <nomdecarte>```. 

J'ai ensuite connecté ma machine en SSH sur node1, marcel et router.

Désormais, il faut créer les routes statiques de node1 et marcel en passant par le routeur, afin de connecter les deux réseaux, et que les machines marcel et node1 puissent communiquer. Pour ce faire, il faut : 
* utiliser la commande ```sudo nano /etc/sysconfig/network-scripts/route-<nomdecarte``` sur marcel et node1, afin de créer des routes statiques définitives reliant les deux réseaux, en utilisant le routeur, via la commande ```<reseauDestination> via <ipCarteRouteur> dev <carteRouteur>```.
* Une fois les routes statiques configurées, il faut executer certaines commandes sur le routeur afin de pouvoir activer la communication entre les deux réseaux : 
```
$ sudo firewall-cmd --list-all
$ sudo firewall-cmd --get-active-zone

$ sudo firewall-cmd --add-masquerade --zone=public
$ sudo firewall-cmd --add-masquerade --zone=public --permanent
```

Une fois ces commandes entrées, il est possible de ping marcel depuis node1, et inversement. Le message sera alors envoyé par la route statique créée vers le routeur, qui le redistribuera ensuite dans le deuxième réseau, où le message sera reçu par la machine cible.

### 2. Analyse de trames.

* On utilise la commande ```sudo ip neigh flush all``` pour vider les tables ARP de node1, marcel et router.

* On fait notre petit ping de node1 vers marcel.

* On attend un peu, on Ctrl-C pour arrêter le ping, et on observe avec ```ip n s``` les tables ARP des trois machines.
Voici les tables ARP des trois machines après le ping de node1 à marcel.
```console
[bob@node1 ~]$ ip n s
10.2.1.15 dev enp0s8 lladdr 0a:00:27:00:00:42 DELAY
10.2.1.254 dev enp0s8 lladdr 08:00:27:c8:07:17 STALE

-----------

[bob@marcel ~]$ ip n s
10.2.2.254 dev enp0s8 lladdr 08:00:27:5f:a7:3d STALE
10.2.2.15 dev enp0s8 lladdr 0a:00:27:00:00:3d DELAY

-----------

[bob@router ~]$ ip n s
10.2.2.11 dev enp0s9  FAILED
10.2.1.11 dev enp0s8 lladdr 08:00:27:44:70:44 STALE
10.2.2.15 dev enp0s9 lladdr 0a:00:27:00:00:3d DELAY
10.2.2.12 dev enp0s9 lladdr 08:00:27:04:34:97 STALE
```
On peut voir que le routeur est impliqué dans chaque intéraction entre node1 et marcel. On peut déduire les échanges ARP entre le routeur et les machines. Par exemple, node1, pour le ping, va envoyer une requête ARP au routeur pour avoir un moyen de communiquer avec l'ip de marcel. Le routeur va lui répondre en lui envoyant l'adresse de la passerelle vers le réseau de marcel. Le routeur enverra ensuite une demande pour savoir à qui envoyer la trame. Marcel lui répondra, et le message de node1 sera transféré à marcel.


| n° | IP Source                |      MAC Source                     |    MAC Destination            | IP Destination            |Protocol        |
|--- |---                       |        ---                      |        ---                        |       ---                 |   ---          |
| 1  |X                         | node1 ```08:00:27:44:70:44```   | Broadcast ```ff:ff:ff:ff:ff:ff``` |    X                      |   ARP          |
| 2  |X                         | marcel ```08:00:27:5f:a7:3d```  | Broadcast ```ff:ff:ff:ff:ff:ff``` |    X                      |   ARP          |
| 3  |X                         | routeur ```08:00:27:5f:a7:3d``` | marcel ```08:00:27:04:34:97```    |    X                      |   ARP          |
| 4  |X                         | routeur ```08:00:27:c8:07:17``` | node1 ```08:00:27:44:70:44```     |    X                      |   ARP          |
| 5  | node1 ```10.2.1.11```    | node 1 ```08:00:27:44:70:44```  | routeur ```08:00:27:c8:07:17```   |  marcel ```10.2.2.12```   |   ICMP (ping)  |
| 6  | routeur ```10.2.2.254``` | routeur ```08:00:27:5f:a7:3d``` | marcel ```08:00:27:04:34:97```    |  marcel ```10.2.2.12```   |   ICMP (ping)  |
| 7  | marcel ```10.2.2.12```   |  marcel ```08:00:27:04:34:97``` | routeur ```08:00:27:5f:a7:3d```   |  routeur ```10.2.2.254``` |   ICMP (pong)  |
| 8  | marcel ```10.2.2.12```   | routeur ```08:00:27:c8:07:17``` | node1 ```08:00:27:44:70:44```     |  node1 ```10.2.2.11```    |   ICMP (pong)  |

### 3. Accès Internet

Comme on a activé la carte NAT sur notre routeur, il a déjà accès à internet. Il nous manque plus qu'à créer sur node1 et marcel des routes par défaut avec le routeur en passerelle.

```console
[bob@node1 ~]$ sudo ip route add default via 10.2.1.254 dev enp0s8
[bob@node1 ~]$ ip r s
default via 10.2.1.254 dev enp0s8
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.11 metric 100
10.2.2.0/24 via 10.2.1.254 dev enp0s8 proto static metric 100

------------------------------------------------------------------------------

[bob@marcel ~]$ sudo ip route add default via 10.2.2.254 dev enp0s8
[sudo] password for bob:
[bob@marcel ~]$ ip r s
default via 10.2.2.254 dev enp0s8
10.2.1.0/24 via 10.2.2.254 dev enp0s8 proto static metric 100
10.2.2.0/24 dev enp0s8 proto kernel scope link src 10.2.2.12 metric 100
```

ping vers une IP :
```console
[bob@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=14.1 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=112 time=16.5 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=112 time=16.8 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=112 time=14.6 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 14.131/15.490/16.758/1.135 ms
```

dig vers Google.com sur Node1. J'ai fait la même chose sur marcel ensuite.

```console
[bob@node1 ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 63920
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             157     IN      A       142.250.179.110

;; Query time: 15 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Wed Sep 29 21:55:08 CEST 2021
;; MSG SIZE  rcvd: 55
```

Désormais, on peut faire un ping vers un nom de domaine.

```console
[bob@node1 ~]$ ping google.com
PING google.com (142.250.75.238) 56(84) bytes of data.
64 bytes from par10s41-in-f14.1e100.net (142.250.75.238): icmp_seq=1 ttl=113 time=14.1 ms
64 bytes from par10s41-in-f14.1e100.net (142.250.75.238): icmp_seq=2 ttl=113 time=13.8 ms
64 bytes from par10s41-in-f14.1e100.net (142.250.75.238): icmp_seq=3 ttl=113 time=17.6 ms
64 bytes from par10s41-in-f14.1e100.net (142.250.75.238): icmp_seq=4 ttl=113 time=14.7 ms
64 bytes from par10s41-in-f14.1e100.net (142.250.75.238): icmp_seq=5 ttl=113 time=16.5 ms
64 bytes from par10s41-in-f14.1e100.net (142.250.75.238): icmp_seq=6 ttl=113 time=15.0 ms
^C
--- google.com ping statistics ---
6 packets transmitted, 6 received, 0% packet loss, time 5008ms
rtt min/avg/max/mdev = 13.813/15.299/17.607/1.354 ms
```

#### Analyse de trames.

| n° | IP Source                |      MAC Source                 |    MAC Destination              | IP Destination       |Protocol         |
|--- |---                       |        ---                      |        ---                      |       ---            |   ---           |
| 1  |   node1 `10.2.1.11`      | node1 ```08:00:27:44:70:44```   | routeur ```08:00:27:c8:07:17``` |  `8.8.8.8`           |   ping (request)|
| 2  |  `8.8.8.8`               | routeur ```08:00:27:c8:07:17``` | node1 ```08:00:27:44:70:44```   |  `10.2.1.11`         |   ping (reply)  |
