# TP Léo Réseau n°1

## Exploration Locale en Solo

1. Affichage d'informations sur la pile TCP/IP locale

**En ligne de commande**

Affichez les infos des cartes réseau de votre PC : 
* Pour l'interface Wi-fi : 
    commande : `ipconfig /all` 
    nom : Carte réseau sans fil wi-fi
    adresse MAC (adresse physique): `A4-B1-C1-72-13-98`
    adresse IP : `10.33.2.208`
    
* Pour l'interface Ethernet : 
    commande : `ipconfig /all`
    nom : Ethernet 
    adresse MAC : `00-2B-67-FE-94-37`
    adresse IP : Impossible de la voir, comme je ne suis pas connecté au réseau.
    
Affichez votre Gateway: 
* J'utilise la même commande qu'au dessus, à savoir `ipconfig /all`, et j'obtiens : `Passerelle par défaut : 10.33.3.253`

**En graphique (GUI : Graphical User Interface)**
* ![](https://i.imgur.com/kKPxBwQ.png)
* Une Gateway sert à connecter deux réseaux distincts.

## Modifications des informations

### A. Modifications d'adresse IP

Il faut aller dans les paramètres réseaux et internet, puis aller dans notre Wifi. En allant à la section Paramètres IP, il est possible de changer soi-même son IP.
![](https://i.imgur.com/7iAw6rp.png)

Si l'adresse IP choisie est déjà utilisée par un autre utilisateur, il est impossible de recevoir la réponse d'une requête envoyée à internet. Comme, lors de la connection, la machine envoie à travers la passerelle une requête à Internet, il est impossible de recevoir la réponse à cette demande. La machine comprend donc qu'elle n'est pas connectée à Internet, d'où la perte de connexion.

### A. Table ARP

```
C:\Users\lucas>arp -a

Interface : 10.33.2.208 --- 0x7
  Adresse Internet      Adresse physique      Type
  10.33.0.35            70-66-55-52-8a-55     dynamique
  10.33.1.71            9c-fc-e8-42-f1-7b     dynamique
  10.33.1.185           2a-eb-09-09-c0-1a     dynamique
  10.33.2.26            e8-d0-fc-99-1e-ed     dynamique
  10.33.2.93            d8-12-65-b5-11-77     dynamique
  10.33.2.132           1c-bf-c0-16-ff-65     dynamique
  10.33.2.145           52-80-48-e2-04-cf     dynamique
  10.33.2.160           de-e6-48-af-e5-d2     dynamique
  10.33.2.222           88-bf-e4-cb-d9-46     dynamique
  10.33.2.237           be-fd-16-82-f4-05     dynamique
  10.33.3.160           a4-83-e7-71-58-57     dynamique
  10.33.3.165           28-d0-ea-f0-bc-61     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 169.254.99.88 --- 0x10
  Adresse Internet      Adresse physique      Type
  169.254.255.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 169.254.132.1 --- 0x13
  Adresse Internet      Adresse physique      Type
  169.254.255.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
```
Pour retrouver l'adresse MAC de la passerelle, il suffit de faire un ipconfig pour retrouver son adresse IP. Il faut ensuite trouver l'adresse MAC attribuée dans la liste de la commande du dessus. Dans mon cas, il s'agit de `10.33.3.253`, et son adresse MAC est : `00-12-00-40-4c-bf`

J'ai pu ping les adresses 10.33.3.219 et 10.33.2.222. 

```
C:\Users\lucas>ping 10.33.3.219

Envoi d’une requête 'Ping'  10.33.3.219 avec 32 octets de données :
Réponse de 10.33.3.219 : octets=32 temps=62 ms TTL=64
Réponse de 10.33.3.219 : octets=32 temps=92 ms TTL=64
Réponse de 10.33.3.219 : octets=32 temps=158 ms TTL=64
Réponse de 10.33.3.219 : octets=32 temps=36 ms TTL=64

Statistiques Ping pour 10.33.3.219:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 36ms, Maximum = 158ms, Moyenne = 87ms

C:\Users\lucas>ping 10.33.2.222

Envoi d’une requête 'Ping'  10.33.2.222 avec 32 octets de données :
Réponse de 10.33.2.222 : octets=32 temps=279 ms TTL=64
Réponse de 10.33.2.222 : octets=32 temps=54 ms TTL=64
Réponse de 10.33.2.222 : octets=32 temps=549 ms TTL=64
Réponse de 10.33.2.222 : octets=32 temps=54 ms TTL=64

Statistiques Ping pour 10.33.2.222:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 54ms, Maximum = 549ms, Moyenne = 234ms
```
    

En regardant dans la table ARP, on peut trouver les adresses MAC correspondantes, à savoir : 
10.33.2.222 = 88-bf-e4-cb-d9-46
10.33.3.219 = a0-78-17-b5-63-bb


# Exploration locale en duo

### 4. Utilisation d'un des deux comme gateway

Après avoir configuré l'adresse IP, on active le partage via l'option de partage sur windows : 

![](https://i.imgur.com/SH0ydDc.png)

On test ensuite l'accès internet avec la commande ping 1.1.1.1 : 

```
Envoi d’une requête 'Ping'  1.1.1.1 avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps=18 ms TTL=58
Réponse de 1.1.1.1 : octets=32 temps=18 ms TTL=58
Réponse de 1.1.1.1 : octets=32 temps=18 ms TTL=58
Réponse de 1.1.1.1 : octets=32 temps=16 ms TTL=58
```

#### Sur le PC qui n'a pas internet

il suffit de configurer l'adresse IP et mettre la passerelle par défaut avec l'adresse IP de l'autre PC : 

```
   [...]
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.6(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . : 192.168.1.5
   [...]
```
et voila