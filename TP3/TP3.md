# TP3 : Progressons vers le réseau d'infrastructure

## 1. Adressage

| Nom du réseau | Adresse du réseau | Masque        | Nombre de clients possibles | Adresse passerelle | [Adresse broadcast](../../cours/lexique/README.md#adresse-de-diffusion-ou-broadcast-address) |
|---------------|-------------------|---------------|-----------------------------|--------------------|----------------------------------------------------------------------------------------------|
| `server1`     | `10.3.0.0`        | /25 (`255.255.255.128`) | 126                        | `10.3.0.126`         | `10.3.0.127`                                                                                   |
| `client1`     | `10.3.0.128`        | /26 (`255.255.255.192`) | 126                           | `10.3.0.190`         | `10.3.0.191`                                                                                   |
| `server2`     | `10.3.0.192`        | /28 (`255.255.255.240`) | 14                          | `10.3.0.206`         | `10.3.0.207` |


| Nom machine  | Adresse de la machine dans le réseau `client1` | Adresse dans `server1` | Adresse dans `server2` | Adresse de passerelle |
|--------------|------------------------------------------------|------------------------|------------------------|-----------------------|
| `router.tp3` | `10.3.0.190/26`                                   | `10.3.0.126/25`           | `10.3.0.206/28`  | Carte NAT             |
| `dns.server1.tp3` | ...                                            | 10.3.0.2                 | ...                    | `10.3.0.126/25`|
| `marcel.client1.tp3` | dhcp (`10.3.0.133`)                         | ...                 | ...                    | `10.3.0.190/26`|
| `dhcp.client1.tp3` | `10.3.0.130`                                  | ...                 | ...                    | `10.3.0.190/26`|

Le routeur a une IP dans chaque réseau.
```console
[bob@router ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00

[...]

3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:98:38:17 brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.126/25 brd 10.3.0.127 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe98:3817/64 scope link noprefixroute
       valid_lft forever preferred_lft forever

4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ab:f3:0d brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.190/26 brd 10.3.0.191 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feab:f30d/64 scope link
       valid_lft forever preferred_lft forever

5: enp0s10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:f0:00:09 brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.206/28 brd 10.3.0.207 scope global noprefixroute enp0s10
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fef0:9/64 scope link
       valid_lft forever preferred_lft forever
```
une résolution de nom.
```console
[bob@router ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 18817
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             221     IN      A       142.250.178.142

;; Query time: 29 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Tue Oct 05 12:00:09 CEST 2021
;; MSG SIZE  rcvd: 55
```

et un accès internet

```console
[bob@router ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=23.9 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=24.1 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 23.861/23.970/24.080/0.189 ms
```

il s'appelle "router.tp3": 

```console
[bob@router ~]$ cat /etc/hostname
router.tp3
```

on active le routage : 

```console
[bob@router ~]$ sudo firewall-cmd --add-masquerade --zone=public
[sudo] password for bob:
success
[bob@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```

# II. Services d'infra

Ce qu'on appelle un "service d'infra" c'est un service qui n'est pas directement utile pour un utilisateur du réseau. Ce n'est pas un truc que l'utilisateur va consommer sciemment, pour obtenir quelque chose.  

Un service d'infra, c'est un service nécessaire pour que l'infra tienne debout, dans des conditions normales de fonctionnement. On trouve, entre autres :

- serveurs DHCP
- serveurs DNS
- serveurs de sauvegarde
- serveurs d'annuaires (Active Directory par exemple)
- serveurs de monitoring (surveillance)

## 1. Serveur DHCP

le serveur DHCP a pour nom dhcp.client1.tp3

```console
[bob@dhcp ~]$ cat /etc/hostname
dhcp.client1.tp3
```

il donne des adresses IP dans le réseau, l'adresse de la passerelle et un serveur DNS

```console
[bob@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
subnet 10.3.0.128 netmask 255.255.255.192 {
    range 10.3.0.131 10.3.0.189;
    option domain-name-servers 1.1.1.1;
    option routers 10.3.0.190;
}
```

Mettre en place un client dans le réseau client1

de son p'tit nom marcel.client1.tp3

```console
[bob@marcel ~]$ cat /etc/hostname
marcel.client1.tp3
```
la machine récupérera une IP dynamiquement grâce au serveur DHCP
ainsi que sa passerelle et une adresse d'un DNS utilisable

PS : Je tiens quand même à souligner que le nom bob@marcel est quand même vachement drôle. C'est vraiment le combo ultra beauf bob + marcel, tout en ressemblant à un très grand artiste un peu foncedé, c'est pépité. Merci d'avoir appelé ta VM marcel.

```console
[bob@marcel ~]$ ip r s
default via 10.3.0.190 dev enp0s8 proto dhcp metric 100

[bob@marcel ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=111 time=15.8 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=111 time=14.6 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=111 time=14.6 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=111 time=16.2 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 14.559/15.311/16.217/0.729 ms

[bob@marcel ~]$ dig google.com
[...]
;; ANSWER SECTION:
google.com.             172     IN      A       142.250.178.142
```

on peut ping 8.8.8.8, et google.com, donc on a un accès internet et un DNS valide.

```console
[bob@marcel ~]$ traceroute 8.8.8.8
traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
 1  _gateway (10.3.0.190)  0.988 ms  0.927 ms  0.902 ms
```

Voici toutes les commandes de la configuration du DNS.
```console
[bob@dns ~]$ sudo vi /etc/named.conf
[bob@dns ~]$ sudo vi /var/named/server1.forward
[bob@dns ~]$ sudo vi /var/named/server2.forward

[bob@dns ~]$ sudo chown root:named /var/named/server1.forward
[bob@dns ~]$ sudo chown root:named /var/named/server2.forward

[bob@dns ~]$ sudo chmod 644 /var/named/server1.forward
[bob@dns ~]$ sudo chmod 644 /var/named/server2.forward

[bob@dns ~]$ sudo vi /etc/resolv.conf
[bob@dns ~]$ cat /etc/resolv.conf
nameserver 10.3.0.2

[bob@dns ~]$ sudo firewall-cmd --add-service=dns --permanent
success
[bob@dns ~]$ sudo firewall-cmd --reload
success

[bob@dns ~]$ sudo named-checkzone server1.tp3 /var/named/server1.forward
zone server1.tp3/IN: loaded serial 2021080804
OK
[bob@dns ~]$ sudo named-checkzone server2.tp3 /var/named/server2.forward
zone server2.tp3/IN: loaded serial 2021080804
OK

[bob@dns ~]$ sudo systemctl enable named.service --now
Created symlink /etc/systemd/system/multi-user.target.wants/named.service → /usr/lib/systemd/system/named.service.
[bob@dns ~]$ sudo systemctl status named
● named.service - Berkeley Internet Name Domain (DNS)
   Loaded: loaded (/usr/lib/systemd/system/named.service; enabled; vendor preset: disabled)
   Active: active (running) since Sun 2021-10-17 22:50:04 CEST; 20s ago
  Process: 1670 ExecStart=/usr/sbin/named -u named -c ${NAMEDCONF} $OPTIONS (code=exited, status=0/SUCCESS)
  Process: 1668 ExecStartPre=/bin/bash -c if [ ! "$DISABLE_ZONE_CHECKING" == "yes" ]; then /usr/sbin/named-checkconf -z "$NAMEDCON> Main PID: 1672 (named)
    Tasks: 4 (limit: 4946)
   Memory: 59.1M
   CGroup: /system.slice/named.service
           └─1672 /usr/sbin/named -u named -c /etc/named.conf
           [...]
```

### Tests depuis Marcel

```console
[bob@marcel ~]$ sudo vi /etc/resolv.conf
[bob@marcel ~]$ cat /etc/resolv.conf
nameserver 10.3.0.2

[bob@marcel ~]$ ping google.com
PING google.com (142.250.74.238) 56(84) bytes of data.
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=1 ttl=111 time=16.3 ms
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=2 ttl=111 time=15.5 ms
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=3 ttl=111 time=14.8 ms
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=5 ttl=111 time=15.3 ms
^C
--- google.com ping statistics ---
5 packets transmitted, 4 received, 20% packet loss, time 4817ms
rtt min/avg/max/mdev = 14.770/15.461/16.274/0.554 ms
```

