# TP4 : Vers un réseau d'entreprise

# I. Dumb switch

## 2. Adressage topologie 1

| Node  | IP            |
|-------|---------------|
| `pc1` | `10.1.1.1/24` |
| `pc2` | `10.1.1.2/24` |

## 3. Setup topologie 1

🌞 **Commençons simple**

- définissez les IPs statiques sur les deux VPCS
On se connecte via telnet avec la commande `telnet <ipMachine> <portMachine>`
```console
PC1> ip 10.1.1.1/24
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0
PC1> save
Saving startup configuration to startup.vpc
.  done

PC2> ip 10.1.1.2/24
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0
PC2> save
Saving startup configuration to startup.vpc
.  done
```

- `ping` un VPCS depuis l'autre

On démarre le switch si il s'il ne s'est pas allumé tout seul, pis on ping.
```console
PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=9.534 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=9.326 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=10.079 ms
84 bytes from 10.1.1.1 icmp_seq=4 ttl=64 time=14.780 ms
84 bytes from 10.1.1.1 icmp_seq=5 ttl=64 time=10.162 ms
```

# II. VLAN

**Le but dans cette partie va être de tester un peu les *VLANs*.**

On va rajouter **un troisième client** qui, bien que dans le même réseau, sera **isolé des autres grâce aux *VLANs***.

**Les *VLANs* sont une configuration à effectuer sur les *switches*.** C'est les *switches* qui effectuent le blocage.

Le principe est simple :

- déclaration du VLAN sur tous les switches
  - un VLAN a forcément un ID (un entier)
  - bonne pratique, on lui met un nom
- sur chaque switch, on définit le VLAN associé à chaque port
  - genre "sur le port 35, c'est un client du VLAN 20 qui est branché"

## 2. Adressage topologie 2

| Node  | IP            | VLAN |
|-------|---------------|------|
| `pc1` | `10.1.1.1/24` | 10   |
| `pc2` | `10.1.1.2/24` | 10   |
| `pc3` | `10.1.1.3/24` | 20   |

### 3. Setup topologie 2

🌞 **Adressage**

- définissez les IPs statiques sur tous les VPCS.
```console
PC3> ip 10.1.1.3/24
Checking for duplicate address...
PC3 : 10.1.1.3 255.255.255.0
PC3> save
Saving startup configuration to startup.vpc
.  done
```
- vérifiez avec des `ping` que tout le monde se ping

```console
PC3> ping 10.1.1.1
84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=6.812 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=11.047 ms
[...]

PC3> ping 10.1.1.2
84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=13.270 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=15.274 ms
[...]
PC3>
```

🌞 **Configuration des VLANs**

- référez-vous à la section VLAN du mémo Cisco.
- déclaration des VLANs sur le switch `sw1`
```console
1005 trnet-default                    act/unsup

VLAN Type  SAID       MTU   Parent RingNo BridgeNo Stp  BrdgMode Trans1 Trans2
---- ----- ---------- ----- ------ ------ -------- ---- -------- ------ ------
1    enet  100001     1500  -      -      -        -    -        0      0
10   enet  100010     1500  -      -      -        -    -        0      0
20   enet  100020     1500  -      -      -        -    -        0      0
1002 fddi  101002     1500  -      -      -        -    -        0      0
1003 tr    101003     1500  -      -      -        -    -        0      0
1004 fdnet 101004     1500  -      -      -        ieee -        0      0
1005 trnet 101005     1500  -      -      -        ibm  -        0      0


Primary Secondary Type              Ports
------- --------- ----------------- ------------------------------------------

```
- ajout des ports du switches dans le bon VLAN (voir [le tableau d'adressage de la topo 2 juste au dessus](#2-adressage-topologie-2))
  - ici, tous les ports sont en mode *access* : ils pointent vers des clients du réseau

```console
Switch#show vlan br

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi0/3, Gi1/0, Gi1/1, Gi1/2
                                                Gi1/3, Gi2/0, Gi2/1, Gi2/2
                                                Gi2/3, Gi3/0, Gi3/1, Gi3/2
                                                Gi3/3
10   admins                           active    Gi0/0, Gi0/1
20   guests                           active    Gi0/2
1002 fddi-default                     act/unsup
1003 token-ring-default               act/unsup
1004 fddinet-default                  act/unsup
1005 trnet-default                    act/unsup
Switch#
```

🌞 **Vérif**

- `pc1` et `pc2` doivent toujours pouvoir se ping

```console
PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=10.611 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=7.903 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=10.607 ms

PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=4.614 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=11.397 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=3.887 ms
```
- `pc3` ne ping plus personne

```console
PC3> ping 10.1.1.1

host (10.1.1.1) not reachable

PC3> ping 10.1.1.2

host (10.1.1.2) not reachable

PC3>
```

# III. Routing

Dans cette partie, on va donner un peu de sens aux VLANs :

- un pour les serveurs du réseau
  - on simulera ça avec un p'tit serveur web
- un pour les admins du réseau
- un pour les autres random clients du réseau

Cela dit, il faut que tout ce beau monde puisse se ping, au moins joindre le réseau des serveurs, pour accéder au super site-web.

**Bien que bloqué au niveau du switch à cause des VLANs, le trafic pourra passer d'un VLAN à l'autre grâce à un routeur.**

Il assurera son job de routeur traditionnel : router entre deux réseaux. Sauf qu'en plus, il gérera le changement de VLAN à la volée.

## 1. Topologie 3

![Topologie 3](./pics/topo3.png)

## 2. Adressage topologie 3

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
|-----------|---------------|--------------|
| `clients` | `10.1.1.0/24` | 11           |
| `admins`  | `10.2.2.0/24` | 12           |
| `servers` | `10.3.3.0/24` | 13           |

L'adresse des machines au sein de ces réseaux :

| Node               | `clients`       | `admins`        | `servers`       |
|--------------------|-----------------|-----------------|-----------------|
| `pc1.clients.tp4`  | `10.1.1.1/24`   | x               | x               |
| `pc2.clients.tp4`  | `10.1.1.2/24`   | x               | x               |
| `adm1.admins.tp4`  | x               | `10.2.2.1/24`   | x               |
| `web1.servers.tp4` | x               | x               | `10.3.3.1/24`   |
| `r1`               | `10.1.1.254/24` | `10.2.2.254/24` | `10.3.3.254/24` |

## 3. Setup topologie 3

🖥️ VM `web1.servers.tp4`, déroulez la [Checklist VM Linux](#checklist-vm-linux) dessus

🌞 **Adressage**

- définissez les IPs statiques sur toutes les machines **sauf le *routeur***

🌞 **Configuration des VLANs**

- référez-vous au [mémo Cisco](../../cours/memo/memo_cisco.md#8-vlan)
- déclaration des VLANs sur le switch `sw1`
- ajout des ports du switches dans le bon VLAN (voir [le tableau d'adressage de la topo 2 juste au dessus](#2-adressage-topologie-2))
- il faudra ajouter le port qui pointe vers le *routeur* comme un *trunk* : c'est un port entre deux équipements réseau (un *switch* et un *routeur*)

```console
sw1# conf t
(config)# interface Gi3/3
(config-if)# switchport trunk encapsulation dot1q
(config-if)# switchport mode trunk
(config-if)# switchport trunk allowed vlan add 11,12,13
(config-if)# exit
(config)# exit
```

configuration des sous-interfaces du routeur R1 :
```console
R1(config)#interface f0/0.11
R1(config-subif)#encapsulation dot1Q 11
R1(config-subif)#ip addr 10.1.1.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface f0/0.12
R1(config-subif)#encapsulation dot1Q 12
R1(config-subif)#ip addr 10.2.2.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface f0/0.13
R1(config-subif)#encapsulation dot1Q 13
R1(config-subif)#ip addr 10.3.3.254 255.255.255.0
R1(config-subif)#exit
```

vérification des configurations des Vlans + routeur

```console
PC1> ping 10.1.1.254

84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=19.262 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=11.486 ms
84 bytes from 10.1.1.254 icmp_seq=3 ttl=255 time=19.461 ms
84 bytes from 10.1.1.254 icmp_seq=4 ttl=255 time=8.696 ms
84 bytes from 10.1.1.254 icmp_seq=5 ttl=255 time=15.183 ms

---

PC2> ping 10.1.1.254

84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=11.796 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=9.042 ms
84 bytes from 10.1.1.254 icmp_seq=3 ttl=255 time=17.531 ms

---

Adm1> ping 10.2.2.254

84 bytes from 10.2.2.254 icmp_seq=1 ttl=255 time=6.420 ms
84 bytes from 10.2.2.254 icmp_seq=2 ttl=255 time=75.329 ms
84 bytes from 10.2.2.254 icmp_seq=3 ttl=255 time=21.950 ms
84 bytes from 10.2.2.254 icmp_seq=4 ttl=255 time=11.443 ms
84 bytes from 10.2.2.254 icmp_seq=5 ttl=255 time=8.916 ms

--- 

web1$ ping 10.3.3.254

64 bytes from 10.3.3.254: icmp_seq=1 ttl=255 time=25.2 ms
64 bytes from 10.3.3.254: icmp_seq=2 ttl=255 time=12.2 ms
64 bytes from 10.3.3.254: icmp_seq=2 ttl=255 time=17.10 ms
```

## 3. Setup topologie 4

🌞 **Ajoutez le noeud Cloud à la topo**

```shell
# Test de ping du routeur vers un serveur DNS public
R1#ping 1.1.1.1

Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 1.1.1.1, timeout is 2 seconds:
.!!!!
Success rate is 80 percent (4/5), round-trip min/avg/max = 40/75/100 ms
```

🌞 **Configurez le NAT**

```shell
# Configuration du NAT
R1(config)#int f0/0
R1(config-if)#ip nat inside
*Mar  1 01:21:47.571: %LINEPROTO-5-UPDOWN: Line protocol on Interface NVI0, changed state to up
R1(config-if)#exit

R1(config)#int f3/0     
R1(config-if)#ip nat outside
R1(config-if)#exit

R1(config)#access-list 1 permit any
R1(config)#ip nat inside source list 1 interface f3/0 overload 
```

🌞 **Test**

```shell
# Ajout des DNS au VPCS
PC1> ip dns 1.1.1.1

# Test final pour PC1 (fonctionne aussi avec tous les autres)
PC1> ping google.com
google.com resolved to 142.250.178.142

84 bytes from 142.250.178.142 icmp_seq=1 ttl=61 time=29.675 ms
84 bytes from 142.250.178.142 icmp_seq=2 ttl=61 time=45.252 ms

# Ajout du DNS à la machine virtuelle
[bob@web ~]$ cat /etc/resolv.conf 
nameserver 1.1.1.1

# Test final pour web1
[bob@web ~]$ ping -c 2 google.com
PING google.com (142.250.178.142) 56(84) bytes of data.
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=1 ttl=61 time=38.8 ms
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=2 ttl=61 time=43.3 ms

--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 38.768/41.041/43.315/2.282 ms
```

## 1. Topologie 5

## 2. Adressage topologie 5

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
|-----------|---------------|--------------|
| `clients` | `10.1.1.0/24` | 11           |
| `servers` | `10.2.2.0/24` | 12           |
| `routers` | `10.3.3.0/24` | 13           |

L'adresse des machines au sein de ces réseaux :

| Node                | `clients`       | `admins`        | `servers`       |
|---------------------|-----------------|-----------------|-----------------|
| `pc1.clients.tp4`   | `10.1.1.1/24`   | x               | x               |
| `pc2.clients.tp4`   | `10.1.1.2/24`   | x               | x               |
| `pc3.clients.tp4`   | DHCP            | x               | x               |
| `pc4.clients.tp4`   | DHCP            | x               | x               |
| `pc5.clients.tp4`   | DHCP            | x               | x               |
| `dhcp1.clients.tp4` | `10.1.1.253/24` | x               | x               |
| `adm1.admins.tp4`   | x               | `10.2.2.1/24`   | x               |
| `web1.servers.tp4`  | x               | x               | `10.3.3.1/24`   |
| `r1`                | `10.1.1.254/24` | `10.2.2.254/24` | `10.3.3.254/24` |

## 3. Setup topologie 5

```shell
# Configuration du serveur DHCP
[bob@dhcp1 ~]$ sudo cat /etc/dhcp/dhcpd.conf
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
default-lease-time 600;
max-lease-time 7200;
authoritative;
subnet 10.1.1.0 netmask 255.255.255.0 {
	range 10.1.1.4 10.1.1.252;
	option routers 10.1.1.254;
	option subnet-mask 255.255.255.0;
	option domain-name-servers 1.1.1.1;
}
```
